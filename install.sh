#!/bin/sh

#
# Install homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

#
# Tap into cask room
brew tap caskroom/cask


#
# Install additional tools:
brew install git 
brew install rsync
brew install ssh-copy-id
brew install wget
brew install zsh zsh-completions

#
# Install applications
brew cask install alfred
brew cask install atom
brew cask install appcleaner
brew cask install dropbox
brew cask install firefox
brew cask install google-chrome
brew cask install java
brew cask install slack
brew cask install google-backup-and-sync
brew cask install 1password
brew cask install sketch
brew cask install adobe-creative-cloud
brew cask install franz
brew cask install spotify

#
# Brew cleanup
brew cleanup --force
rm -f -r /Library/Caches/Homebrew/*

#
# Build a global gitignore file
curl https://raw.githubusercontent.com/github/gitignore/master/Global/JetBrains.gitignore >> ~/.gitignore_global
curl https://raw.githubusercontent.com/github/gitignore/master/Composer.gitignore >> ~/.gitignore_global
curl https://raw.githubusercontent.com/github/gitignore/master/Sass.gitignore >> ~/.gitignore_global
curl https://raw.githubusercontent.com/github/gitignore/master/Global/macOS.gitignore >> ~/.gitignore_global
curl https://raw.githubusercontent.com/github/gitignore/master/Global/Vim.gitignore >> ~/.gitignore_global
curl https://raw.githubusercontent.com/github/gitignore/master/Global/Redis.gitignore >> ~/.gitignore_global
curl https://raw.githubusercontent.com/github/gitignore/master/Global/Dropbox.gitignore >> ~/.gitignore_global
curl https://raw.githubusercontent.com/github/gitignore/master/Global/GPG.gitignore >> ~/.gitignore_global
git config --global core.excludesfile '~/.gitignore_global'

#
# Configure macOS
defaults write -g AppleInterfaceStyle -string 'Dark'
defaults write com.apple.dock magnification -bool false
defaults write com.apple.dock tilesize -int 40
defaults write com.apple.screensaver askForPassword -int 1
defaults write com.apple.screensaver askForPasswordDelay -int 0
defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false
defaults write com.apple.finder EmptyTrashSecurely -bool true
defaults write com.apple.finder ShowStatusBar -bool false
defaults write com.apple.finder ShowPathbar -bool true

#
# Killall affected apps
for app in "Dock" "Finder" "SystemUIServer"; do
  killall "${app}" > /dev/null 2>&1
done
